#include "main.hpp"
#define rcast reinterpret_cast
// зачем вот это писать "рэинтерпрет_каст"? ркаст

unsigned int bones[] = { SPINE, HEAD, LEFT_SHOULDER, RIGHT_SHOULDER, NECK, JAW };
hook_shared_t<void(*)()> hk_game_loop;

bool hk_rpc(unsigned char &id, BitStream *&&bs) {
	if (id == RPC_ScrSetPlayerAttachedObject) {
		bool create;
		bs->IgnoreBits(48);
		bs->Read(create);
		if (create) {
			bs->IgnoreBits(32);
			unsigned int bone;
			bs->Read(bone);

			size_t b_size = sizeof(bones) / sizeof(unsigned int);

			for (size_t i = 0; i < b_size; i++) {
				if (bones[i] == bone) {
					bs->ResetReadPointer();
					return false;
				}
			}
		}
		bs->ResetReadPointer();
	}
	return true;
};

void game_loop() {
	hk_game_loop->call_original();

	static bool initialized = false;
	if (initialized || !rakhook::initialize()) 
		return;

	rakhook::on_receive_rpc += hk_rpc;

	initialized = true;
}

class fuck_hats {
public:
	fuck_hats() {
		rakhook::samp_version version = rakhook::get_samp_version();
		if (!rakhook::samp_addr() || version == rakhook::SAMP_UNKNOWN) 
			return;

		hk_game_loop = std::make_shared<hook_t<void(*)()>>(rcast<void(*)()>(0x53BEE0), game_loop);
	}
} _fuck_hats;